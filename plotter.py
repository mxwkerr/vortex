import numpy as np
import matplotlib.pyplot as plt
import ast

with open("vortex.txt","r") as f:
   data = np.array(ast.literal_eval(f.readlines()[0]))
   sz = int(np.sqrt(len(data)))
   data = data.reshape((sz,sz))

plt.imshow(data, interpolation="nearest", cmap='hsv')
plt.show()


with open("aperture.txt","r") as f:
   data = np.array(ast.literal_eval(f.readlines()[0]))
   sz = int(np.sqrt(len(data)))
   data = data.reshape((sz,sz))
plt.imshow(data, interpolation="nearest", cmap='binary')
plt.show()