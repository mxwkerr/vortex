import System.IO
import System.Random
import Math.FFT.Base
--import Data.Fixed
--import Data.Complex
import Wavetools

aperture :: Float -> Float -> Integer 
aperture x y = if norm [x-5,y-5] <= 3 then 0 else 1


-- remember archinux needs dynamic linking
-- insall with "cabal install --lib --ghc-options=-dynamic pkg_name"
  
main :: IO ()
main = do
  g <- getStdGen


  -- n random plane waves
  writeFile "vortex.txt" (show ([ phaseComplex (buildNRandomWaves g 10 [x,y,0]) | x <- [0,0.1..10], y <- [0,0.1..10] ] ))

  -- aperture
  writeFile "aperture.txt" (show ([ aperture x y | x <- [0,0.01..10], y <- [0,0.01..10] ]))

  -- FT of aperture
  writeFile "FTaperture.txt" (show ( [ (aperture x y) | x <- [0,0.01..10], y <- [0,0.01..10] ]))

