module Wavetools
( dot,
  phaseComplex,
  buildNRandomWaves,
  norm)
where

-- Imports
import System.Random
import Data.List.Split
import Data.List

-- DEFINING SOME HELPER FUNCTIONS
-- Define a dot product
dot :: [Float] -> [Float] -> Float
dot k r = sum(zipWith(*) k r)

norm :: [Float] -> Float
norm x = sum [ i**2 | i <- x]


-- Get phase of a complex number
phaseComplex :: (Float, Float) -> Float
phaseComplex z = 2 * atan (im /(sqrt(im**2 + re**2) + re)) + (3.14/2) where (re, im) = z


-- DATA GENERATING FUNCTIONS

-- Given a StdGen and No of waves, returns the random waves
buildNRandomWaves :: StdGen -> Int -> ([Float] -> (Float, Float))
buildNRandomWaves g n =
  let ks = chunksOf 3 (take (n*3) (randomRs (-1.0, 1.0) g))
  in (\ r -> ( sum([cos (dot k r) | k <- ks]), sum([sin (dot k r) | k <- ks] )))